# distutils: language = c++
import numpy as np
# from queue import Queue
from numpy.core.umath_tests import inner1d
np.random.seed(1)

from libcpp.queue cimport queue
from libcpp.vector cimport vector
cimport numpy as np
cimport cython

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

D_INT32 = np.int32
ctypedef np.int32_t D_INT32_T

D_INT8 = np.uint8
ctypedef np.uint8_t D_INT8_T 

cdef extern from "math.h":
    double c_sqrt "sqrt"(double)


@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
cdef void get_normal_c(np.ndarray[DTYPE_t, ndim=1] vv1, np.ndarray[DTYPE_t, ndim=1] vv2, np.ndarray[DTYPE_t, ndim=1] vv3, double[:] ans):
    cdef double u1, u2, u3, v1, v2, v3, norm

    u1 = vv2[0] - vv1[0]
    u2 = vv2[1] - vv1[1]
    u3 = vv2[2] - vv1[2]

    v1 = vv3[0] - vv1[0]
    v2 = vv3[1] - vv1[1]
    v3 = vv3[2] - vv1[2]

    # print(u1, u2, u3, v1, v2, v3)

    ans[0] = u2 * v3 - u3 * v2
    ans[1] = u3 * v1 - u1 * v3
    ans[2] = u1 * v2 - u2 * v1

    norm = c_sqrt(ans[0] * ans[0] + ans[1] * ans[1] + ans[2] * ans[2])
    ans[0] = ans[0] / norm
    ans[1] = ans[1] / norm
    ans[2] = ans[2] / norm
    # TODO do normalization

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def get_normals_c(np.ndarray[DTYPE_t, ndim=3] triangle_points):
    """Gets the normals of a numpy array containing the coordinates of triangles

    Arguments:
        points {ndarray} -- Shape is NX3X3

    Returns:
        ndarray -- Shape is NX3
    """
    cdef int n_triangles = triangle_points.shape[0]
    cdef double[:, :] norms_ = np.zeros([n_triangles, 3], dtype=DTYPE)
    cdef double[:] temp = np.zeros([3], dtype=DTYPE)
    for i in range(0, n_triangles):
        get_normal_c(triangle_points[i, 0, :], triangle_points[i, 1, :], triangle_points[i, 2, :], temp)
        norms_[i, 0] = temp[0]
        norms_[i, 1] = temp[1]
        norms_[i, 2] = temp[2]
    return norms_

def get_normals(triangle_points):
    norms_ = get_normals_c(triangle_points) 
    return norms_
    #return norms_ / np.expand_dims(np.sqrt(inner1d(norms_,norms_)), axis=1)


def search_sorted_improved(arr, val):
    """Search a sorted array, return None if not found

    Arguments:
        arr {ndarray} -- Numpy Array
        val {T} -- value to compare and find

    Returns:
        int,None -- index of value or None
    """

    idx = np.searchsorted(arr, val, side='left')
    if idx < arr.shape[0] and arr[idx] == val:
        return idx
    else:
        return None

cdef void vector_to_array(vector[int] &vec, np.ndarray[D_INT32_T, ndim=1] to_copy):
    cdef D_INT32_T[:] to_copy_view = to_copy
    for i in range(vec.size()):
        to_copy_view[i] = vec[i]


def extract_mesh(tri, np.ndarray[D_INT32_T, ndim=1] global_indices, in_set_flag, D_INT32_T seed_idx):
    """Will extract a mesh from a possible set of triangles that have a similar normals.
    Extraction is started at the seed_idx of the set of triangles. Indices provided in global_indices of all possible
    triangles that have similar normals. Indices correspond to tri.simplices array.

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together
        in_set_flag {ndarray[bool]} -- An ndarray of bools of same size of global_indices.
            Bool indicates if triangle is removed from set.
        seed_idx {int} -- index of triangle that will be the seed of this mesh extraction

    Returns:
        ndarray -- Array of indices of triangles all spatially connected that have similar normals
    """
    cdef queue[int] candidates_que  # Queue for use in triangle neighbor expansion
    candidates_que.push(seed_idx)
    # Like Removing from set
    in_set_flag[search_sorted_improved(global_indices, seed_idx)] = False
    # candidates = []
    cdef vector[int] candidates
    cdef int i = 0
    cdef int[:] neighbors
    while not candidates_que.empty():
        global_idx = candidates_que.front()  # triangle index
        candidates_que.pop()
        candidates.push_back(global_idx)

        # LOGGER.debug("Triangle idx: %d", global_idx)
        neighbors = tri.neighbors[global_idx]
        # LOGGER.debug("Neighbors: %s", neighbors)
        for i in range(0, 3):
            if neighbors[i] == -1:
                continue
            local_idx = search_sorted_improved(global_indices, neighbors[i])
            if local_idx is not None and in_set_flag[local_idx]:
                # This neighbor triangle is in global_indices and has a similar normal
                # LOGGER.debug(
                #     'Neighbor %d found in tri_set. Adding to candidate set', neighbor)
                candidates_que.push(neighbors[i])
                # Remove from set
                in_set_flag[local_idx] = False
    cdef np.ndarray to_copy = np.zeros([candidates.size()], dtype=D_INT32)
    vector_to_array(candidates, to_copy)
    return to_copy

def create_planes(tri, global_indices, min_triangles=20, seed=1):
    """Extract a list of mesh planes from a set of triangles

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together

    Keyword Arguments:
        min_triangles {int} -- Minimum number of triangles to consider a plane patch (default: {10})

    Returns:
        list(ndarray) -- A list of an array of indices of triangles all spatially connected that have similar normals
    """

    random_gen = np.random.RandomState(seed=seed)

    # global_i
    ndices = global_indices.copy()
    in_set_flag = np.full(global_indices.shape, True,
                          dtype=bool)  # in set flag

    plane_patches = []  # candidate extracted mesh planes
    # Iterate until every triangle has been expored in global_indices
    while np.any(in_set_flag):
        # Only explore triangles that are still in the set
        filtered_indices = global_indices[in_set_flag]
        # Choose a triangle from this filtered set
        seed_idx = filtered_indices[random_gen.choice(
            filtered_indices.shape[0])]
        # Attempt to extract a plane patch using this triangle seed
        plane_patch = extract_mesh(tri, global_indices, in_set_flag, seed_idx)
        # Check if enough triangles are in this patch
        if plane_patch.shape[0] > min_triangles:
            plane_patches.append(plane_patch)

    return plane_patches