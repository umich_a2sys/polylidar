"""Functions to extract planar meshes from a point cloud
"""

import logging
from queue import Queue
import time


from scipy.spatial import Delaunay  # pylint: disable=E0611
import numpy as np
from shapely.ops import cascaded_union
from shapely.prepared import prep
from shapely.geometry import asPolygon, Point, multipolygon
from shapely.affinity import translate
from polylidar.polylabel import polylabel
LOGGER = logging.getLogger('polylidar')
LOGGER.setLevel(logging.INFO)
# Try to import cython code
speed = None


M_TO_FT = 3.28

try:
    import polylidar.pyximportcpp
    polylidar.pyximportcpp.install(setup_args={'include_dirs': np.get_include()})
    import polylidar.speed as speed
except:
    LOGGER.info('Falling back to pure python code')
else:
    globals()['speed'] = speed
    LOGGER.info('Using Fast Cython code')

# try:
#     print('help')
#     from polylidar import speed
# except Exception as e:
#     print('problem', e)

def get_poly_coords(poly):
    """Extract coordinates from a shapely geometry
    
    Arguments:
        poly {shapely.Geometry} -- Shapely Geometry
    
    Returns:
        list -- List of coordinates
    """

    coords = [poly.exterior.coords[:]]
    for interior in list(poly.interiors):
        coords.append(list(interior.coords))
    return coords

def get_normal(points):
    """Gets the normal of a triangle

    Arguments:
        points {ndarray} -- Numpy array of size 3

    Returns:
        ndarray -- Normal of triangle surface
    """

    line1 = points[0, :] - points[1, :]
    line2 = points[1, :] - points[2, :]
    normal1 = np.cross(line1, line2)
    normal1_n = normal1 / np.linalg.norm(normal1)
    return normal1_n


def get_normals(points):
    """Gets the normals of a numpy array containing the coordinates of triangles

    Arguments:
        points {ndarray} -- Shape is NX3X3

    Returns:
        ndarray -- Shape is NX3
    """

    norms_ = np.zeros((points.shape[0], 3))
    for i in range(0, points.shape[0]):
        norms_[i, :] = get_normal(points[i, :, :])
    return norms_


def search_sorted_improved(arr, val):
    """Search a sorted array, return None if not found

    Arguments:
        arr {ndarray} -- Numpy Array
        val {T} -- value to compare and find

    Returns:
        int,None -- index of value or None
    """

    idx = np.searchsorted(arr, val, side='left')
    if idx < arr.shape[0] and arr[idx] == val:
        return idx
    else:
        return None


def filter_triangles(tri, points, norms, desired_vector=np.array([0, 0, 1]), norm_thresh=.98, z_thresh=.15, x_thresh=2, y_thresh=2, **kwargs):
    """Returns an ndarray of triangle INDICES whose normals are within norm_thresh of desired_vector
    Pass in the norms of the triangles and the desired normal vector.  Dot product will be made and any triangle
    whose norm is to far off will be filtered.  Some small noisy triangles may be filtered, so a z_thresh is set so that
    flat triangles are not filtered out.

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        points {ndarray} -- NX3, the Delaunay object has only the 2D points but we need the 3D points
        norms {ndarray} -- NX3

    Keyword Arguments:
        desired_vector {ndarray} -- Normal vector we want the triangles to filter on (default: {np.array([0, 0, 1])})
        norm_thresh {float} -- How close the dot product of the triangle norm and desired vector
            be (greater than) (default: {.98})
        z_thresh {float} -- All triangles whose range of z-cord is less than z_thresh are not filtered (default: {.15})

    Returns:
        ndarray -- MX1; where M is amount of leftover triangles
    """
    mask_small_x = np.ptp(points[tri.simplices][:, :, 0], axis=1) < x_thresh
    mask_small_y = np.ptp(points[tri.simplices][:, :, 1], axis=1) < y_thresh

    mask_small = np.ptp(points[tri.simplices][:, :, 2], axis=1) < z_thresh
    mask_norm = np.abs(np.dot(norms, desired_vector)) > norm_thresh

    global_indices = np.arange(tri.nsimplex, dtype=np.int32)
    # large triangles removed, only small triangles or those with correct normal left
    global_indices = global_indices[(mask_small_x & mask_small_y) & (mask_small | mask_norm)]
    return global_indices

# @profile
def extract_mesh(tri, global_indices, in_set_flag, seed_idx):
    """Will extract a mesh from a possible set of triangles that have a similar normals.
    Extraction is started at the seed_idx of the set of triangles. Indices provided in global_indices of all possible
    triangles that have similar normals. Indices correspond to tri.simplices array.

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together
        in_set_flag {ndarray[bool]} -- An ndarray of bools of same size of global_indices.
            Bool indicates if triangle is removed from set.
        seed_idx {int} -- index of triangle that will be the seed of this mesh extraction

    Returns:
        ndarray -- Array of indices of triangles all spatially connected that have similar normals
    """
    candidates_que = Queue()  # Queue for use in triangle neighbor expansion
    candidates_que.put(seed_idx)
    # Like Removing from set
    in_set_flag[search_sorted_improved(global_indices, seed_idx)] = False
    candidates = []
    while not candidates_que.empty():
        global_idx = candidates_que.get()  # triangle index
        candidates.append(global_idx)

        LOGGER.debug("Triangle idx: %d", global_idx)
        neighbors = tri.neighbors[global_idx]
        LOGGER.debug("Neighbors: %s", neighbors)
        for neighbor in neighbors:
            local_idx = search_sorted_improved(global_indices, neighbor)
            if local_idx is not None and in_set_flag[local_idx]:
                # This neighbor triangle is in global_indices and has a similar normal
                LOGGER.debug(
                    'Neighbor %d found in tri_set. Adding to candidate set', neighbor)
                candidates_que.put(neighbor)
                # Remove from set
                in_set_flag[local_idx] = False
            else:
                LOGGER.debug("Neighbor %d not found", neighbor)

    return np.array(candidates)

# @profile
def create_planes(tri, global_indices, min_triangles=20, seed=1):
    """Extract a list of mesh planes from a set of triangles

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together

    Keyword Arguments:
        min_triangles {int} -- Minimum number of triangles to consider a plane patch (default: {10})

    Returns:
        list(ndarray) -- A list of an array of indices of triangles all spatially connected that have similar normals
    """

    random_gen = np.random.RandomState(seed=seed)
    global_indices = global_indices.copy()
    in_set_flag = np.full(global_indices.shape, True,
                          dtype=bool)  # in set flag

    plane_patches = []  # candidate extracted mesh planes
    # Iterate until every triangle has been expored in global_indices
    while np.any(in_set_flag):
        # Only explore triangles that are still in the set
        filtered_indices = global_indices[in_set_flag]
        # Choose a triangle from this filtered set
        seed_idx = filtered_indices[random_gen.choice(
            filtered_indices.shape[0])]
        LOGGER.debug("Seed Triangle idx: %d", seed_idx)
        # Attempt to extract a plane patch using this triangle seed
        plane_patch = extract_mesh(tri, global_indices, in_set_flag, seed_idx)
        # Check if enough triangles are in this patch
        if plane_patch.shape[0] > min_triangles:
            plane_patches.append(plane_patch)
        else:
            LOGGER.debug('Candidate set has two few triangles, discarding')
    return plane_patches


def filter_planes(tri, plane_patches, min_bbox_area=16):
    """Filter a list of possible planes

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        plane_patches {list(ndarray)} -- A list of an array of indices of triangles all spatially
            connected that have similar normals

    Keyword Arguments:
        min_bbox_area {int} -- Minimum bbox of area of polygon (default: {16})

    Returns:
        list(ndarray) -- A list of an array of indices of triangles all spatially connected that have similar normals
    """

    filtered_patches = []
    for patch in plane_patches:
        patch_points = tri.points[tri.simplices[patch]]
        patch_points = patch_points.reshape(-1, patch_points.shape[-1])
        mins = np.min(patch_points, axis=0)
        maxs = np.max(patch_points, axis=0)
        bbox = (maxs[0] - mins[0]) * (maxs[1] - mins[1])
        if bbox >= min_bbox_area:
            filtered_patches.append(patch)
    return filtered_patches

# @profile
def extract_planes(pointcloud, desired_vector=np.array([0, 0, 1]), norm_thresh=.98, z_thresh=.15, min_triangles=20, min_bbox_area=16, **kwargs):
    """Extracts planes from a point cloud
    Returns a list tuple, where the first element is a list of ndarray of indices of triangles of an extracted plane
    The second element contains the Delaunay object to match indices of triangles and points

    Arguments:
        pointcloud {ndarray} -- Point cloud array, NX3

    Keyword Arguments:
        desired_vector {ndarray} -- normal vector of plane to extract (default: {np.array([0, 0, 1])})
        norm_thresh {float} -- [description] (default: {.98})
        z_thresh {float} -- [description] (default: {.15})
        min_triangles {int} -- [description] (default: {20})
        min_bbox_area {int} -- Minimum bbox of area of polygon (default: {16})

    Returns:
        list(ndarray), tri -- list of ndarray of indices of triangles, A Delaunay object of triangles
    """
    # Delaunay will operate only on the projected 2D points of the pointcloud
    points_2d = pointcloud[:, 0:2] - pointcloud[:, 0:2].min(axis=0)
    # 3D points of building, removing previously computed normals from WBT
    points = pointcloud[:, 0:3]
    # Perfrom triangulation, get normals, filter triangles to have same normal requested
    tri = Delaunay(points_2d, qhull_options="Qbb Qc Qz Qx Q12 Qt")
    points_vertices = points[tri.simplices]  # Points of vertices
    # points_vertices = points_vertices.copy()
    # print(points_vertices.shape, points_vertices.dtype, points_vertices.flags)
    # t0 = time.time()
    norms = speed.get_normals(points_vertices) if speed else get_normals(points_vertices)
    # t1 = time.time()
    # print("Time diff {:.4f}".format(t1 - t0))
    global_indices = filter_triangles(
        tri, points, norms, desired_vector=desired_vector, norm_thresh=norm_thresh, z_thresh=z_thresh, **kwargs)

    if speed:
        plane_patches = speed.create_planes(tri, global_indices, min_triangles=min_triangles)
    else:
        plane_patches = create_planes(tri, global_indices, min_triangles=min_triangles)

    plane_patches = filter_planes(
        tri, plane_patches, min_bbox_area=min_bbox_area)

    return plane_patches, tri, global_indices


# TODO: Add average height to polygon as well
def mesh_to_polygons(tri, patch, min_area=16, negative_buffer=-0.25, simplify_buffer=0.25, **kwargs):
    """Converts a triangular mesh to a 2D polygon(s)
    If polygon has holes they will be widened by .5 meters, may cut the polygon into two!
    Each polygon must have at least a min area.

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        patch {ndarray} -- ndarray of indices of triangles

    Keyword Arguments:
        min_area {int} -- Minimum area of a polygon (default: {16})

    Returns:
        shapely -- List of shapely geometries, may be only a size of 0, 1, or more
    """

    shapely_triangles = []
    final_geometries = []
    patch = tri.simplices[patch]

    # TODO: Optimize this by createing the polygon myself and not using cascaded_union
    # Create Shapely Geometries
    for i in range(patch.shape[0]):
        tri_points = np.array(tri.points[patch[i, :]])
        # must add first point again to "close" the polygon
        shapely_tri = np.vstack((tri_points, tri_points[0, :]))
        shapely_triangles.append(asPolygon(shapely_tri))

    building_polygon = cascaded_union(shapely_triangles)

    if building_polygon.area >= min_area:
        smoothed_building_polygon = building_polygon.buffer(negative_buffer).simplify(simplify_buffer)
        if smoothed_building_polygon.type == 'MultiPolygon':
            for polygon in smoothed_building_polygon.geoms: # pylint: disable=E1101
                if polygon.area >= min_area:
                    final_geometries.append(polygon)
        else:
            final_geometries.append(smoothed_building_polygon)

    return final_geometries


def plane_meshes_to_polygons(tri, patch_planes, points=None, min_area=16, **kwargs):
    """Converts a list triangular meshes to a list of 2D polygons
    If polygon has holes they will be widened by .5 meters, may cut the polygon into two!
    Each polygon must have at least a min area.

    Arguments:
        tri {delaunay} -- A Delaunay object of points that has already been triangulated
        patch_planes {ndarray} -- list of ndarray of indices of triangles

    Keyword Arguments:
        min_area {int} -- Minimum area of a polygon (default: {16})

    Returns:
        list(dicts) -- List of dictionary with shapely geometries, may be only a size of 0, 1, or more
    """
    
    geometries = []
    # If the original point cloud has been passed, then we should translate the geometries by min point offsets
    # For numerical stability, the points were originally subtracted by each axis mins to generate the meshes.
    if points is not None:
        offsets = points[:, 0:2].min(axis=0)

    for patch_id, patch in enumerate(patch_planes):
        new_geoms = mesh_to_polygons(tri, patch, min_area=min_area, **kwargs)
        patch_height = None
        if points is not None:
            # Only look at one triangle to get height, they should all have the same height anyways
            patch_height = points[tri.simplices[patch[0]]][0][2]
            # translate the polygon
            new_geoms = list(map(lambda geom: translate(geom, xoff=offsets[0], yoff=offsets[1]) , new_geoms))
        new_geoms = list(map(lambda geom: dict(geometry=geom, height=patch_height, patch_id=patch_id), new_geoms))
        geometries.extend(new_geoms)
    return geometries


def polygon_check2(polygon, min_area=16, **kwargs):
    """Ensures that multipolygons are broken into polygons and have a certain min area
    
    Arguments:
        polygon {shapely} -- Shapely polygon
    
    Keyword Arguments:
        min_area {int} -- Min area of polygon (default: {16})
    
    Returns:
        list[shapely] -- List of shapely polygons
    """

    polygons = []
    if isinstance(polygon, multipolygon.MultiPolygon):
        polygons.extend(list(polygon))
    else:
        polygons.append(polygon)
    polygons = list(filter(lambda x: x.area > min_area, polygons))
    return polygons
    
        
def iterate_polylabel(geom, min_radius=2, **kwargs):
    """Finds greatest inscribed circle of a polygon using poylabel.
    This function will then SUBTRACT this circle from the original polygon
    and attempt to find the next largest circle. This process repeats until no more
    circles can ben found that have a min_radius.

    
    Arguments:
        geom {dictionary} -- Dictionary with geometry keyword holding shapely geom
    
    Keyword Arguments:
        min_radius {int} -- Minimum radius of circle needed to keep expanding a shape (default: {2})
    
    Returns:
        list(dict) -- List of dictionaries containing radius and center of circles
    """

    geoms = [geom['geometry']]
    list_centers = []
    while geoms:
        geom_ = geoms.pop(0)
        (center, radius, _) = polylabel(polylidar.get_poly_coords(geom_))
        # print(center, radius)
        if radius > min_radius:
            center_point = Point(center).buffer(radius)
            list_centers.append(dict(geom, geometry=center_point, center=center, radius=radius))
            reduced_geom = polygon_check2(geom_.difference(center_point), **kwargs)
            geoms.extend(reduced_geom)
    return list_centers

def find_all_circles(planar_geometries, **kwargs):
    """Finds all the largest inscribed circles inside the list of planar geometries
    
    Arguments:
        planar_geometries {list[dict]} -- list of dictionary that contains a geometry object   
    
    Returns:
        list[dict] -- List of dictionary with circle information
    """

    landing_circles = []
    for geom in planar_geometries:
        landing_circles.extend(iterate_polylabel(geom, **kwargs))
    return landing_circles
