# TODO

1. Optimize creation of Shapely triangle and cascaded_union
2. Remove triangles that are to long. These are obviously incorrect and are stretched out. [Example](../assets/bad_connection.jpg)
3. 



## Timing

Building 0:

Extract Planes took: 2.27141 seconds
Extract Polygons: 2.92322 seconds


Building 1:

Extract Planes took: 0.04565 seconds
Extract Polygons: 0.15326 seconds

## Profiling

Example profiling for Building 0, Baseline

Embarrassingly simple optimizations can be made to get_normals() and create_planes().  Just use cython for these
```
Total time: 2.23463 s
File: C:\Users\Jerem\Documents\UMICH\Research\polylidar\polylidar\__init__.py
Function: extract_planes at line 207

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
   207                                           @profile
   208                                           def extract_planes(pointcloud, desired_vector=np.array([0, 0, 1]), norm_thresh=.98, z_thresh=.15, min_triangles=20, min_bbox_area=16):
   209                                               """Extracts planes from a point cloud
   210                                               Returns a list tuple, where the first element is a list of ndarray of indices of triangles of an extracted plane
   211                                               The second element contains the Delaunay object to match indices of triangles and points
   212
   213                                               Arguments:
   214                                                   pointcloud {ndarray} -- Point cloud array, NX3
   215
   216                                               Keyword Arguments:
   217                                                   desired_vector {ndarray} -- normal vector of plane to extract (default: {np.array([0, 0, 1])})
   218                                                   norm_thresh {float} -- [description] (default: {.98})
   219                                                   z_thresh {float} -- [description] (default: {.15})
   220                                                   min_triangles {int} -- [description] (default: {20})
   221                                                   min_bbox_area {int} -- Minimum bbox of area of polygon (default: {16})
   222
   223                                               Returns:
   224                                                   list(ndarray), tri -- list of ndarray of indices of triangles, A Delaunay object of triangles
   225                                               """
   226
   227                                               # Delaunay will operate only on the projected 2D points of the pointcloud
   228         1        578.0    578.0      0.0      points_2d = pointcloud[:, 0:2] - pointcloud[:, 0:2].min(axis=0)
   229                                               # 3D points of building, removing previously computed normals from WBT
   230         1          5.0      5.0      0.0      points = pointcloud[:, 0:3]
   231                                               # Perfrom triangulation, get normals, filter triangles to have same normal requested
   232         1      83532.0  83532.0      1.5      tri = Delaunay(points_2d, qhull_options="Qbb Qc Qz Qx Q12 Qt")
   233         1       3839.0   3839.0      0.1      points_vertices = points[tri.simplices]  # Points of vertices
   234         1    4179897.0 4179897.0     73.9      norms = get_normals(points_vertices)
   235         1          4.0      4.0      0.0      global_indices = filter_triangles(
   236         1      17898.0  17898.0      0.3          tri, points, norms, desired_vector=desired_vector, norm_thresh=norm_thresh, z_thresh=z_thresh)
   237
   238         1          5.0      5.0      0.0      plane_patches = create_planes(
   239         1    1365173.0 1365173.0     24.1          tri, global_indices, min_triangles=min_triangles)
   240
   241         1          4.0      4.0      0.0      plane_patches = filter_planes(
   242         1       5483.0   5483.0      0.1          tri, plane_patches, min_bbox_area=min_bbox_area)
   243
   244         1          2.0      2.0      0.0      return plane_patches, tri

```


### First Optimization
Cython Norms

```
$ kernprof -v -l profile_script.py
Running Profile/Timing script
Num Points: (6654, 6)
Wrote profile results to profile_script.py.lprof
Timer unit: 3.95061e-07 s

Total time: 0.571372 s
File: C:\Users\Jerem\Documents\UMICH\Research\polylidar\polylidar\__init__.py
Function: extract_planes at line 210

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
   210                                           @profile
   211                                           def extract_planes(pointcloud, desired_vector=np.array([0, 0, 1]), norm_thresh=.98, z_thresh=.15, min_triangles=20, min_bbox_area=16):
   212                                               """Extracts planes from a point cloud
   213                                               Returns a list tuple, where the first element is a list of ndarray of indices of triangles of an extracted plane
   214                                               The second element contains the Delaunay object to match indices of triangles and points
   215
   216                                               Arguments:
   217                                                   pointcloud {ndarray} -- Point cloud array, NX3
   218
   219                                               Keyword Arguments:
   220                                                   desired_vector {ndarray} -- normal vector of plane to extract (default: {np.array([0, 0, 1])})
   221                                                   norm_thresh {float} -- [description] (default: {.98})
   222                                                   z_thresh {float} -- [description] (default: {.15})
   223                                                   min_triangles {int} -- [description] (default: {20})
   224                                                   min_bbox_area {int} -- Minimum bbox of area of polygon (default: {16})
   225
   226                                               Returns:
   227                                                   list(ndarray), tri -- list of ndarray of indices of triangles, A Delaunay object of triangles
   228                                               """
   229
   230                                               # Delaunay will operate only on the projected 2D points of the pointcloud
   231         1       1038.0   1038.0      0.1      points_2d = pointcloud[:, 0:2] - pointcloud[:, 0:2].min(axis=0)
   232                                               # 3D points of building, removing previously computed normals from WBT
   233         1          7.0      7.0      0.0      points = pointcloud[:, 0:3]
   234                                               # Perfrom triangulation, get normals, filter triangles to have same normal requested
   235         1      74207.0  74207.0      5.1      tri = Delaunay(points_2d, qhull_options="Qbb Qc Qz Qx Q12 Qt")
   236         1       2960.0   2960.0      0.2      points_vertices = points[tri.simplices]  # Points of vertices
   237                                               # points_vertices = points_vertices.copy()
   238                                               # print(points_vertices.shape, points_vertices.dtype, points_vertices.flags)
   239                                               # t0 = time.time()
   240         1      85884.0  85884.0      5.9      norms = speed.get_normals_fast(points_vertices)
   241                                               # t1 = time.time()
   242                                               # print("Time diff {:.4f}".format(t1 - t0))
   243         1          7.0      7.0      0.0      global_indices = filter_triangles(
   244         1      14299.0  14299.0      1.0          tri, points, norms, desired_vector=desired_vector, norm_thresh=norm_thresh, z_thresh=z_thresh)
   245
   246         1          4.0      4.0      0.0      plane_patches = create_planes(
   247         1    1262065.0 1262065.0     87.3          tri, global_indices, min_triangles=min_triangles)
   248
   249         1          4.0      4.0      0.0      plane_patches = filter_planes(
   250         1       5807.0   5807.0      0.4          tri, plane_patches, min_bbox_area=min_bbox_area)
   251
   252         1          4.0      4.0      0.0      return plane_patches, tri

```


### Looking at plane extraction

I could only make it about twice as fast....booo. Maybe an optimization can be made for search sorted numpy

```
$ kernprof -v -l profile_script.py
Running Profile/Timing script
Num Points: (6654, 6)
Wrote profile results to profile_script.py.lprof
Timer unit: 3.95061e-07 s

Total time: 0.539902 s
File: C:\Users\Jerem\Documents\UMICH\Research\polylidar\polylidar\__init__.py
Function: extract_mesh at line 103

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
   103                                           @profile
   104                                           def extract_mesh(tri, global_indices, in_set_flag, seed_idx):
   105                                               """Will extract a mesh from a possible set of triangles that have a similar normals.
   106                                               Extraction is started at the seed_idx of the set of triangles. Indices provided in global_indices of all possible
   107                                               triangles that have similar normals. Indices correspond to tri.simplices array.
   108
   109                                               Arguments:
   110                                                   tri {delaunay} -- A Delaunay object of points that has already been triangulated
   111                                                   global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together
   112                                                   in_set_flag {ndarray[bool]} -- An ndarray of bools of same size of global_indices.
   113                                                       Bool indicates if triangle is removed from set.
   114                                                   seed_idx {int} -- index of triangle that will be the seed of this mesh extraction
   115
   116                                               Returns:
   117                                                   ndarray -- Array of indices of triangles all spatially connected that have similar normals
   118                                               """
   119
   120       144       9423.0     65.4      0.7      candidates_que = Queue()  # Queue for use in triangle neighbor expansion
   121       144       3375.0     23.4      0.2      candidates_que.put(seed_idx)
   122                                               # Like Removing from set
   123       144       3256.0     22.6      0.2      in_set_flag[search_sorted_improved(global_indices, seed_idx)] = False
   124       144        215.0      1.5      0.0      candidates = []
   125      9474      54786.0      5.8      4.0      while not candidates_que.empty():
   126      9330     181910.0     19.5     13.3          global_idx = candidates_que.get()  # triangle index
   127      9330      16500.0      1.8      1.2          candidates.append(global_idx)
   128
   129      9330      63996.0      6.9      4.7          LOGGER.debug("Triangle idx: %d", global_idx)
   130      9330      27765.0      3.0      2.0          neighbors = tri.neighbors[global_idx]
   131      9330      59391.0      6.4      4.3          LOGGER.debug("Neighbors: %s", neighbors)
   132     37320      82738.0      2.2      6.1          for neighbor in neighbors:
   133     27990     407832.0     14.6     29.8              local_idx = search_sorted_improved(global_indices, neighbor)
   134     27990      52954.0      1.9      3.9              if local_idx is not None and in_set_flag[local_idx]:
   135                                                           # This neighbor triangle is in global_indices and has a similar normal
   136      9186      14144.0      1.5      1.0                  LOGGER.debug(
   137      9186      59520.0      6.5      4.4                      'Neighbor %d found in tri_set. Adding to candidate set', neighbor)
   138      9186     178702.0     19.5     13.1                  candidates_que.put(neighbor)
   139                                                           # Remove from set
   140      9186      18706.0      2.0      1.4                  in_set_flag[local_idx] = False
   141                                                       else:
   142     18804     127841.0      6.8      9.4                  LOGGER.debug("Neighbor %d not found", neighbor)
   143
   144       144       3573.0     24.8      0.3      return np.array(candidates)

Total time: 0.653332 s
File: C:\Users\Jerem\Documents\UMICH\Research\polylidar\polylidar\__init__.py
Function: create_planes at line 146

Line #      Hits         Time  Per Hit   % Time  Line Contents
==============================================================
   146                                           @profile
   147                                           def create_planes(tri, global_indices, min_triangles=20):
   148                                               """Extract a list of mesh planes from a set of triangles
   149
   150                                               Arguments:
   151                                                   tri {delaunay} -- A Delaunay object of points that has already been triangulated
   152                                                   global_indices {ndarray} -- A list of INDICES of triangles that could *possibly* be grouped together
   153
   154                                               Keyword Arguments:
   155                                                   min_triangles {int} -- Minimum number of triangles to consider a plane patch (default: {10})
   156
   157                                               Returns:
   158                                                   list(ndarray) -- A list of an array of indices of triangles all spatially connected that have similar normals
   159                                               """
   160
   161         1         20.0     20.0      0.0      global_indices = global_indices.copy()
   162         1          5.0      5.0      0.0      in_set_flag = np.full(global_indices.shape, True,
   163         1         33.0     33.0      0.0                            dtype=bool)  # in set flag
   164
   165         1          2.0      2.0      0.0      plane_patches = []  # candidate extracted mesh planes
   166                                               # Iterate until every triangle has been expored in global_indices
   167       145       3723.0     25.7      0.2      while np.any(in_set_flag):
   168                                                   # Only explore triangles that are still in the set
   169       144       2825.0     19.6      0.2          filtered_indices = global_indices[in_set_flag]
   170                                                   # Choose a triangle from this filtered set
   171       144        266.0      1.8      0.0          seed_idx = filtered_indices[np.random.choice(
   172       144       3675.0     25.5      0.2              filtered_indices.shape[0])]
   173       144       1100.0      7.6      0.1          LOGGER.debug("Seed Triangle idx: %d", seed_idx)
   174                                                   # Attempt to extract a plane patch using this triangle seed
   175       144    1640700.0  11393.8     99.2          plane_patch = extract_mesh(tri, global_indices, in_set_flag, seed_idx)
   176                                                   # Check if enough triangles are in this patch
   177       144        375.0      2.6      0.0          if plane_patch.shape[0] > min_triangles:
   178        19         36.0      1.9      0.0              plane_patches.append(plane_patch)
   179                                                   else:
   180       125        987.0      7.9      0.1              LOGGER.debug('Candidate set has two few triangles, discarding')
   181         1          2.0      2.0      0.0      return plane_patches

Closing LAS File
```

Extracting Polygons will be more difficult