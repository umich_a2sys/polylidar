from setuptools import setup, find_packages
setup(
    name="polylidar",
    version="0.2.1",
    # geo_utils will NOT function unless extra requires develop is selected
    packages=['polylidar', 'geo_utils'],
    scripts=[],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['docutils>=0.3', 'scipy>=1.0', 'numpy>=1.10', 'shapely>=1.5'],

    # Optional install allows fast code
    extras_require = {
        'Cython':  ["Cython"],
        'develop': ['laspy', 'Cython', 'matplotlib', 'descartes', 'seaborn', 'pyproj']
    },
    package_data={
        # If any package contains *.txt or *.rst files, include them:
        'polylidar': ['*.txt', '*.rst', '*.md', '*.pyx'],
    },


    # metadata to display on PyPI
    author="Jeremy Castagno",
    author_email="jdcasta@umich.edu",
    description="Extracts flat polygon areas from lidar points cloud for buildings",
    license="MIT",
    keywords="lidar las polygon pia",
    url="https://bitbucket.org/umich_a2sys/polylidar/src/master/",   # project home page, if any
    project_urls={
        "Bug Tracker": "https://bitbucket.org/umich_a2sys/polylidar/src/master/",
    }

)