import json
from geo_utils.osm import OSM
from geo_utils.lidar import Lidar
import numpy as np

MEAN_SUBTRACT = False
FILTER = False

def main():
    with open('data/annarbor.json') as f:
        example = json.load(f)
    # OSM data connection
    osm = OSM(bbox=example['bbox'], **example['osm'], projection_local=example['lidar']['projection'])
    # LIDAR data file connection
    lidar = Lidar(proj_osm=osm.proj, **example['lidar'])
    # Extract the flat building from OSM
    flat_buildings = [building for building in osm.tables[osm.table_name]
                      if building['area'] > 25]

    for j, building in enumerate(flat_buildings):
        if j >10:
            break
        # get lidar point cloud
        lidar_building, poly = lidar.get_lidar_building(building,filter_points=FILTER)
        if MEAN_SUBTRACT:
            points_3d = lidar_building[:, 0:3] - lidar_building[:, 0:3].min(axis=0)
        else:
            points_3d = lidar_building[:, 0:3]
        np.savetxt('data/buildings/{}.csv'.format(building['uid']), points_3d, fmt='%.4f', delimiter=',')
    
if __name__ == '__main__':
    main()