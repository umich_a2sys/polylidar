using PyCall
using Makie
@pyimport julia

function zoom_scene(scene, zoom=1.0f0)
    camera =cameracontrols(scene)
    
    @extractvalue camera (fov, near, projectiontype, lookat, eyeposition, upvector)
    dir_vector = eyeposition - lookat
    new_eyeposition = lookat + dir_vector * (2.0f0 - zoom)
    update_cam!(scene, new_eyeposition, lookat)
end

buildings = julia.get_lidar_filtering_data();



(lidar_area, lidar_building_uncropped, lidar_building, poly)= buildings[:__next__]();

# (lidar_building, poly, plane_patches, tri, geometries) = buildings[:__next__]();


# for (lidar_building, poly, plane_patches, tri, geometries) in buildings
#     if lidar_building != nothing
#         points_scaled = hcat(lidar_building[:,1] .- minimum(lidar_building[:,1]), lidar_building[:,2] .- minimum(lidar_building[:,2]), lidar_building[:,3] .- minimum(lidar_building[:,3]))
#         scene = scatter(points_scaled[:,1], points_scaled[:,2], points_scaled[:,3])
#         display(scene)
#         println("Hit Enter for the next")
#         _ = readline()
       
#     end
# end


# function Makie.to_mesh(b, m::typeof(lakemesh)) 
#     vertices = map(1:size(m.node, 1)) do i
#         Point3f0(ntuple(j-> m.node[i, j], Val{2})..., 0)
#     end
#     triangles = map(1:size(m.elem, 1)) do i
#         GLTriangle(Int.(ntuple(j-> m.elem[i, j], Val{3})))
#     end
#     GLNormalMesh(vertices, triangles)
# end
