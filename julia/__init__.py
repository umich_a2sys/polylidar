from os import path
import copy
import json

import polylidar
from geo_utils.osm import OSM
from geo_utils.lidar import Lidar
from shapely import geometry
from geo_utils import create_poly


DIR_NAME = path.dirname(path.abspath(__file__))
PARENT_DIR = path.dirname(DIR_NAME)


with open(path.join(PARENT_DIR, 'data', 'example.json')) as f:
    example = json.load(f)

# OSM data connection
osm = OSM(bbox=example['bbox'], **example['osm'])
# LIDAR data file connection
lidar = Lidar(proj_osm=osm.proj, **example['lidar'])
# Extract the flat building from OSM
flat_buildings = [building for building in osm.tables['osm_buildings']
                  if building['roof_shape'] == 'flat' and building['area'] > 25]


def get_building():
    for j, building in enumerate(flat_buildings):
        # get lidar point cloud
        lidar_building, poly = lidar.get_lidar_building(building)
        if lidar_building is None:
            continue
        plane_patches, tri, _ = polylidar.extract_planes(lidar_building)
        geometries = polylidar.plane_meshes_to_polygons(tri, plane_patches)
        yield [lidar_building, poly, plane_patches, tri, geometries]


def get_lidar_filtering_data():
    for j, building in enumerate(flat_buildings):
        # get lidar point cloud
        # make copy of building
        box = copy.deepcopy(building)
        box['wkb'] = geometry.box(
            *(create_poly(box).buffer(50).bounds)).to_wkb()
        lidar_area, poly = lidar.get_lidar_building(box, crop=False)

        lidar_building_uncropped, poly = lidar.get_lidar_building(
            building, crop=False)
        lidar_building, poly = lidar.get_lidar_building(building, crop=True)
        if lidar_building is None:
            continue
        results = [lidar_area - lidar_area.min(axis=0), lidar_building_uncropped - lidar_area.min(axis=0),
                   lidar_building - lidar_area.min(axis=0), poly]
        yield results
