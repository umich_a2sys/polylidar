from os.path import dirname
from sys import path
import json
import timeit
from timeit import default_timer as timer
from functools import partial
import logging

path.insert(0, dirname(dirname(__file__)))
logging.basicConfig(level=logging.INFO)

import polylidar
import geo_utils
from geo_utils.osm import OSM
from geo_utils.lidar import Lidar

print("Running Profile/Timing script")


with open('data/example.json') as f:
    example = json.load(f)

# OSM data connection
osm = OSM(bbox=example['bbox'], **example['osm'])
# LIDAR data file connection
lidar = Lidar(proj_osm=osm.proj, **example['lidar'])
# Extract the flat building from OSM
flat_buildings = [building for building in osm.tables['osm_buildings']
                    if building['roof_shape'] == 'flat' and building['area'] > 25]

building = flat_buildings[0]
# get lidar point cloud
lidar_building, poly = lidar.get_lidar_building(building)
print("Num Points: {}".format(lidar_building.shape))
plane_patches, tri, _ = polylidar.extract_planes(lidar_building)


# START COMMENTING OUT HERE (TIMING CODE) TO PROFILE
# Dont forget to add @profile decorators
# Timer

times = timeit.Timer(partial(polylidar.extract_planes, lidar_building)).repeat(3, 3)
time_taken = min(times) / 3
print("Extract Planes took: {:.5f}".format(time_taken))


times = timeit.Timer(partial(polylidar.plane_meshes_to_polygons, tri, plane_patches)).repeat(3, 3)
time_taken = min(times) / 3
print("Extract Polygons: {:.5f}".format(time_taken))


#
# END COMMENTING HERE TO PROFILE



# kernprof -v -l profile_script.py
