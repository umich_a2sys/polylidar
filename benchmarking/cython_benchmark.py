from os.path import dirname
from sys import path
import timeit
from functools import partial
import logging
import pstats, cProfile

path.insert(0, dirname(dirname(__file__)))
# logging.getLogger('polylidar').setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO)

import numpy as np
from scipy.spatial import Delaunay  # pylint: disable=E0611
import polylidar
from polylidar import speed


np.random.seed(1)

def create_point_cloud(start, stop, height=0, noise=.1):
    dim_range = int(stop - start)
    x = np.expand_dims(
        np.repeat(np.arange(start, stop, step=.5), repeats=dim_range*2), axis=1)
    y = np.expand_dims(
        np.tile(np.arange(start, stop, step=.5), reps=dim_range*2), axis=1)

    actual_size = y.shape[0]
    z = np.random.randn(actual_size, 1) * noise + height
    return np.concatenate((x, y, z), axis=1)


N = 13290
test_array = np.random.randn(N, 3, 3)

print("Get Normals Benchmarks...")
ground_truth = polylidar.get_normals(test_array)
times = timeit.Timer(partial(polylidar.get_normals, test_array)).repeat(3, 3)
time_taken = min(times) / 3
print("Python/Numpy: {:.5f}".format(time_taken))


result = speed.get_normals(test_array)
times = timeit.Timer(partial(speed.get_normals, test_array)).repeat(3, 3)
time_taken = min(times) / 3
print("Cython/Numpy: {:.5f}".format(time_taken))

print('Difference in result (should be 0)')
print((ground_truth - result).sum())


print()
print('Create Planes Benchmark')
# Set up
points = np.vstack((create_point_cloud(0, 40, height=0, noise=.007), create_point_cloud(40, 80, height=2, noise=.007)))
print("Point Cloud Size {}".format(points.shape[0]))
points_2d = points[:, 0:2]
# Perfrom triangulation, get normals, filter triangles to have same normal requested
tri = Delaunay(points_2d, qhull_options="Qbb Qc Qz Qx Q12 Qt")
points_vertices = points[tri.simplices]  # Points of vertices
norms = speed.get_normals(points_vertices)
global_indices = polylidar.filter_triangles(tri, points, norms)
# End Setup
print("Number of Triangles {}".format(global_indices.shape[0]))
ground_truth = np.sort(np.concatenate(polylidar.create_planes(tri, global_indices), axis=0).ravel())
# np.savetxt('ground_truth.txt', np.concatenate(polylidar.create_planes(tri, global_indices), axis=0 ))
times = timeit.Timer(partial(polylidar.create_planes,
                             tri, global_indices)).repeat(3, 3)
time_taken = min(times) / 3
print("Python/Numpy: {:.5f}".format(time_taken))

# cProfile.runctx("speed.create_planes(tri, global_indices)", globals(), locals(), "Profile.prof")
# s = pstats.Stats("Profile.prof")
# s.strip_dirs().sort_stats("time").print_stats()

times = timeit.Timer(partial(speed.create_planes,
                             tri, global_indices)).repeat(3, 3)
time_taken = min(times) / 3
print("Cython/Numpy: {:.5f}".format(time_taken))

result = np.sort(np.concatenate(speed.create_planes(tri, global_indices), axis=0).ravel())
# np.savetxt('result.txt', np.concatenate(speed.create_planes(tri, global_indices), axis=0 ))
print("Difference in result (should be 0): {}".format((ground_truth - result).sum()))
