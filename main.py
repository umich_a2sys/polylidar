"""Script to demo polylidar
"""
import json
import argparse

from matplotlib import pyplot as plt
from matplotlib import colors
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.patches as mpatches
import seaborn as sns
from descartes import PolygonPatch
from polylidar.polylabel import polylabel
# sns.set_style('darkgrid')

import polylidar
import geo_utils
from geo_utils.osm import OSM
from geo_utils.lidar import Lidar


COLOR_PALETTE = sns.color_palette()
# print(len(COLOR_PALETTE))
M_TO_FT = 3.28

def plot_geometries(ax, geometries, min_radius=2, precision=.25,**kwargs):
    # print(min_radius_)
    for i, geometry in enumerate(geometries):
        geom = geometry['geometry']
        patch_id = geometry['patch_id']
        # if the patch id is given use it
        if patch_id is not None:
            i = patch_id

        if i >= len(COLOR_PALETTE):
            i = i % len(COLOR_PALETTE)
        # print(i)
        patch = PolygonPatch(
            geom, color=COLOR_PALETTE[i], zorder=1)
    
        coords = polylidar.get_poly_coords(geom)
        (center, radius, boxes) = polylabel(coords, precision=precision)
        # print(center, radius)
        # Ensure the landing spot is big enough
        if radius < min_radius:
            print("Radius of {} too small. Need a radius of {} ".format(radius, min_radius))
            continue

        circle = mpatches.Circle(center, radius, ec="black", fill=False, zorder=10)
        marker = mpatches.Circle(center, .1, ec="black", color='black', fill=True, zorder=11)
        ax.add_patch(patch)
        ax.add_patch(circle)
        ax.add_patch(marker)
    ax.axis('scaled')

def parse_args():
    parser = argparse.ArgumentParser(description='Show Polylidar')
    parser.add_argument('--file', type=str, default='data/annarbor.json', help='JSON config file')
    parser.add_argument('--all_buildings', action='store_true', help='If set, no filtering of flat buildings occurs')
    args = parser.parse_args()
    print(args)
    return args
def main():
    args = parse_args()
    with open(args.file) as f:
        example = json.load(f)

    # OSM data connection
    osm = OSM(bbox=example['bbox'], **example['osm'], projection_local=example['lidar']['projection'])
    # LIDAR data file connection
    lidar = Lidar(proj_osm=osm.proj, **example['lidar'])
    # Extract the flat building from OSM
    if not args.all_buildings:
        flat_buildings = [building for building in osm.tables[osm.table_name]
                          if building['roof_shape'] == 'flat' and building['area'] > 25]
    else:
        print('All buildings')
        flat_buildings = [building for building in osm.tables[osm.table_name] if building['area'] > 25]

    for j, building in enumerate(flat_buildings):
        if j < 7:
            continue
        if building['uid'] != 314799721:
            continue
        # get lidar point cloud
        lidar_building, poly = lidar.get_lidar_building(building)
        # get plane patches
        plane_patches, tri, _ = polylidar.extract_planes(lidar_building, **example['polylidar'])

        # create a normalized color map
        colormap = plt.get_cmap("viridis")
        vmax = max(lidar_building[:, 2].ptp(), 10 if not example['lidar'].get('preserve_units', False) else 32)
        norm = colors.Normalize(
            vmin=0, vmax=vmax)

        # gridspec_kw = dict(height_ratios=[1,1], width_ratios=[.40, .60])
        # fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(16, 16),
        #                        subplot_kw=dict(projection='3d'))
        fig = plt.figure(figsize=(10, 8))
        ax0 = plt.subplot2grid((2, 8), (0, 1), colspan=6, projection='3d')
        ax1 = plt.subplot2grid((2, 8), (1, 0), colspan=4,  projection='3d')
        ax2 = plt.subplot2grid((2, 8), (1, 4), colspan=4)
        # Plot LIDAR point cloud
        p = ax0.scatter(*geo_utils.scale_points(lidar_building),
                        c=colormap(norm(geo_utils.scale_points(lidar_building)[2])))
        # Plot each plane patch
        fake_lines = []  # need fake lines for legend. Legend not supported in 3D mesh plots
        for i, patch_set in enumerate(plane_patches):
            # print(i)
            if i >= len(COLOR_PALETTE):
                i = i % len(COLOR_PALETTE)
            # print(i)
            patch = tri.simplices[patch_set]
            ax1.plot_trisurf(*geo_utils.scale_points(lidar_building[:, 0:3]),
                             triangles=patch[:, :3], color=COLOR_PALETTE[i])

            fake_lines.append(plt.Line2D(
                [0], [0], linestyle="none", color=COLOR_PALETTE[i], marker='o'))

        for ax_ in [ax0, ax1]:
            ax_.view_init(elev=55., azim=-90)
            ax_.set_zlim(0, vmax)
            # ax_.axis('scaled')
            # ax_.colorbar()

        # Plot Legend
        fake_labels = ["Patch {}".format(i + 1)
                       for i in range(len(plane_patches))]
        ax1.legend(fake_lines, fake_labels, loc=2)

        # Plot color bar
        sm = plt.cm.ScalarMappable(norm=norm, cmap=colormap)
        sm.set_array(colormap(norm(geo_utils.scale_points(lidar_building)[2])))
        fig.colorbar(sm, ax=ax0)
        fig.suptitle('Building id: {}, centroid: {}'.format(
            building['uid'], building['centroid']), fontsize=16, y=1.0)
        fig.tight_layout()

        geometries = polylidar.plane_meshes_to_polygons(tri, plane_patches, **example['polylidar'])
        plot_geometries(ax2, geometries, **example['polylidar'])

        print('Building id: {}, centroid: {}'.format(
            building['uid'], building['centroid']))
        plt.show()


if __name__ == '__main__':
    main()
