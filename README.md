# Polylidar

This module will search for flat **spatially** connected planes in LIDAR points cloud.  Spatially connected means that if a large flat plane is separated by a vertical wall that **two** planes will be extracted, one on each side of the wall. This module returns a polygon representation for each of the flat planes and its corresponding z elevation. This module can be used with `polylabel`, which finds the largest inscribed circles inside the returned polygons.

The motivation of this work is about finding the best emergency landing spots inside urban cities (flat roofs free from clutter).

![Example](assets/example.png)

## Method

This work is inspired from planar roof extraction work done [here](https://www.tandfonline.com/doi/full/10.1080/01431161.2017.1302112). The referenced work outlines an algorithm for extracting planer roof patches. The basic idea is to perform Delaunay triangulation, determine triangular normals, group by normals, and iteratively grow planes from random seeds by expanding spatially connected triangular neighbors.  The result are individual planar meshes that are spatially connected.  

Note that many times triangular holes will appear in the mesh using the papers proposed algorithm, which the papers authors attempt to resolve by looking at neighbors of holes and conditionally filling. However, I have modified the algorithm so that small triangles are not thrown out, even if their normals 'appear' to be abnormal.  I also filter out large triangles which have no meaning for spatial connectivity for a given LIDAR point cloud resolution. All of this is configurable through parameters.

## Installation

Simply `git clone` then `pip install .[Cython]`. The module `polylidar` will then be available for you to use.  Look at src code for documentation but the main functions are:

1. `plane_patches, tri = polylidar.extract_planes(point_cloud)` - Converts point cloud to separated planar patches.
2. `geometries = polylidar.plane_meshes_to_polygons(tri, plane_patches)` - Converts planar patches into shapely polygons.

This module makes use of `scipy` for Delaunay triangulation.  It makes use of `Cython` (compiled c-like python) for some time intensive function that were profiled, however a Python fallback can be used if `Cython` is not installed.

Windows may require conda to install `shapely`.

## Demo

All code specific to polylidar is inside the `polylidar` folder.  The `util` contain utility classes used to test and demo the polylidar package. Their only purpose is to provide a point cloud of building data to polylidar to do its work and plotting. They contain the following - 

1. `diptest` - A way to asses the multi-modality of a distribution. Used to filter some lidar points.
2. `db.py` - Class to connect to spatialte database containing building information.
3. `lidar.py` - Class to handle reading LAS files and getting building point clouds.
4. `osm.py` - Class to handle specific OSM coordinate system information.


The `main.py` script runs the demo of polylidar.  It presents graphs of the point cloud, extracted triangular mesh, and the resulting polygon and greatest inscribed circle. There are several more dependencies that must be installed to run the demo; they can be installed with the `setup.py` with `pip install .[devel]`. The `diptest` dependecy must be installed manually.

The  `benchmarking` folder holds scripts for to benchmark and profile polylidar.  The `profile_script.py` script is used only to help profile/time code, while `cython_benchmark.py` benchmarks pure python versions with Cython versions.



## Other

Poles of inaccessibility work found in [polyabel](https://github.com/mapbox/polylabel).
