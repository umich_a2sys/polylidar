# Shapely Imports

from shapely.geometry import mapping, box, multipolygon
from shapely import wkb, ops, affinity

import numpy as np
from matplotlib.path import Path

from geo_utils.diptest import diptest


def polygon_check(poly):
    if isinstance(poly, multipolygon.MultiPolygon):
        return list(poly.geoms)[0]
    else:
        return poly


def create_poly(building):
    """Creates a polygon from a building

    Arguments:
        building {sqlite row} -- SQLite row containing 'wkb' field

    Returns:
        [poly] -- Shapely Polygon
    """

    return polygon_check(wkb.loads(building['wkb']))


def poly_mask_only(poly, lidar_building_xyz):
    """ Masks out lidar points outside of polygon, return mask """
    vertices = poly.exterior.coords
    path1 = Path(vertices)
    poly_mask_ = path1.contains_points(lidar_building_xyz[:, 0:2])
    return poly_mask_


def poly_mask(poly, lidar_building_xyz, points=None):
    """ Masks out lidar points outside of polygon, returns points """
    points_ = points if points is not None else lidar_building_xyz
    poly_mask_ = poly_mask_only(poly, lidar_building_xyz)
    return points_[poly_mask_]


def scale_points(lidar_points, bounded_lidar_points=None, legacy=True):
    points = bounded_lidar_points if bounded_lidar_points is not None else lidar_points
    min_x = np.min(points[:, 0])
    min_y = np.min(points[:, 1])
    min_z = np.min(points[:, 2])
    max_z = np.max(points[:, 2])

    # if max_z - min_z < 10:
    #     min_z = max_z - 10

    xp2p = points[:, 0].ptp()
    yp2p = points[:, 1].ptp()
    if xp2p > yp2p:
        max_z = xp2p
    else:
        max_z = yp2p

    x = points[:, 0] - min_x
    y = points[:, 1] - min_y
    z = points[:, 2] - min_z
    if legacy:
        return x, y, z
    else:
        return dict(x=x, y=y, z=z, extent=[[0, xp2p], [0, yp2p], [0 - max_z, max_z]])


def is_not_outlier(points, thresh=3.0, median=None):
    """
    Returns a boolean array with True if points are outliers and False
    otherwise.

    Parameters:
    -----------
        points : An num observations by num dimensions array of observations
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.

    Returns:
    --------
        mask : A num observations-length boolean array.

    References:
    ----------
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points.shape) == 1:
        points = points[:, None]
    if median is None:
        median = np.median(points, axis=0)

    diff = np.sum((points - median)**2, axis=-1)
    diff = np.sqrt(diff)
    med_abs_deviation = np.median(diff)

    modified_z_score = 0.6745 * diff / med_abs_deviation
    return modified_z_score < thresh


def is_uni_modal(points):
    """ Determines if the samples come from a unimodal distribution using the diptest """
    dip, p_value = diptest.diptest(points)
    return not (dip > .04 and p_value < .2)
