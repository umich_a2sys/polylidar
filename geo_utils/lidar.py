
import atexit
from functools import partial
import logging

import numpy as np
import pyproj
from laspy.file import File
from shapely import ops

import geo_utils

M_TO_FT = 3.28
LOOSE_FILTER_THRESHOLD = 15
MIN_POINTS = 65


class Lidar(object):
    """ Class that parses osm and lidar data """

    def __init__(self, file_path, projection, proj_osm, **kwargs):
        self.lidar_feet = kwargs.get('preserve_units', False)
        self.file_path = file_path
        self.proj = pyproj.Proj(init=projection, preserve_units=self.lidar_feet)
        self.proj_osm = proj_osm
        self.crs_local = int(projection.split(':')[1])

        # Open the Lidar data
        self.las = None
        las = File(self.file_path, mode="r")
        self.las = las
        if self.las.header.data_format_id >= 2:
            # Lets store the normal data as well
            normals = np.stack(
                (self.las.Red, self.las.Green, self.las.Blue), axis=-1)
            normals = normals.astype(np.float)
            normals2 = normals * 2 / (255 * 256) - 1.0
            mask_overlap_ground = (self.las.Classification != 12) & (self.las.Classification != 2)
            self.lidar = np.stack((self.las.x, self.las.y, self.las.z,
                                   normals2[:, 0], normals2[:, 1], normals2[:, 2]), axis=-1)
            self.lidar = self.lidar[mask_overlap_ground]
            self.lidar_x = self.lidar.view(
                dtype=[('x', np.float64), ('y', np.float64), ('z', np.float64), ('r', np.float64), ('g', np.float64), ('b', np.float64)])
        else:
            # Only XYZ Points
            self.lidar = np.stack(
                (self.las.x, self.las.y, self.las.z), axis=-1)
            mask_overlap_ground = (self.las.Classification != 12) & (self.las.Classification != 2)
            self.lidar = self.lidar[mask_overlap_ground]
            self.lidar_x = self.lidar.view(
                dtype=[('x', np.float64), ('y', np.float64), ('z', np.float64)])

        self.lidar_x.sort(order=['x'], axis=0)
        self.lidar_x = self.lidar_x['x'].reshape((self.lidar_x.shape[0],))

        self.osm_to_lidar = partial(
            pyproj.transform,
            self.proj_osm,
            self.proj)

        def close_all():
            if las:
                print("Closing LAS File")
                las.close()

        # atexit.register(close_all)

    def __del__(self):
        """Remove LIDAR file resource"""
        if self.las:
            print("Closing LAS File")
            self.las.close()

    def fast_lidar_mask(self, bounds):
        """ Rapidly Masks Out LIDAR Points outside of bounds """
        x_min, y_min, x_max, y_max = bounds
        x_index_bounds = np.searchsorted(self.lidar_x, [x_min, x_max])
        x_points = self.lidar[x_index_bounds[0]:x_index_bounds[1]]
        filt_mask = np.all([(x_points[:, 1] > y_min),
                            (x_points[:, 1] < y_max)], axis=0)
        lidar_building = x_points[filt_mask]
        return lidar_building

    def get_ground_height(self, poly, buffer_dist=1.5):
        buffer_dist_m = buffer_dist * M_TO_FT if self.lidar_feet else buffer_dist
        expanded_poly = poly.buffer(buffer_dist_m)
        lidar_building = self.fast_lidar_mask(expanded_poly.bounds)
        return min(lidar_building[:, 2])

    def filter_ground_points(self, lidar_building, poly, height_buffer=2.0):
        height_buffer_m = height_buffer * M_TO_FT if self.lidar_feet else height_buffer
        min_height = self.get_ground_height(poly)
        mask = lidar_building[:, 2] > min_height + height_buffer_m
        return lidar_building[mask]

    def get_lidar_building(self, building, auto_scale=False, crop=True, filter_points=True, percent_filter=0, filter_dist=2, outlier_thresh=3.0):
        poly = geo_utils.create_poly(building)
        poly = geo_utils.polygon_check(poly)
        poly = ops.transform(self.osm_to_lidar, poly)
        # Fast Lidar filtering in bbox of polygon
        lidar_building = self.fast_lidar_mask(poly.bounds)
        # Slow filter for points outside of the polygon

        if crop and filter_points:
            lidar_building = geo_utils.poly_mask(poly, lidar_building)

            if lidar_building.shape[0] < MIN_POINTS:
                logging.warning("Not enough points for lidar building")
                return None, None

            # Filter ground points
            lidar_building = self.filter_ground_points(lidar_building, poly)
            # first just remove the top percent outliers
            upper = np.percentile(lidar_building[:, 2], 100.0 - percent_filter)
            lower = np.percentile(lidar_building[:, 2], percent_filter)
            lidar_building = lidar_building[(lidar_building[:, 2] < upper) & (
                lidar_building[:, 2] > lower)]
            # Now remove outliers using Medium Absolute Deviation technique IF the distribution is unimodal
            # Check Unimodality with dip test
            is_uni = geo_utils.is_uni_modal(lidar_building[:, 2])
            if is_uni:
                filter_dist_m = filter_dist * M_TO_FT if self.lidar_feet else filter_dist
                # Remove Outliers
                outlier_mask = geo_utils.is_not_outlier(
                    lidar_building[:, 2], thresh=outlier_thresh)
                try:
                    if filter_dist_m is not None:
                        # shrunk_scale = self.get_scale(poly, filter_dist, expand=False)
                        # shrunk_poly = affinity.scale(poly, xfact=shrunk_scale, yfact=shrunk_scale)
                        shrunk_poly = poly.buffer(-filter_dist_m)
                        poly_mask = geo_utils.poly_mask_only(
                            shrunk_poly, lidar_building)
                        outlier_mask = (outlier_mask) | (poly_mask)
                        lidar_building = lidar_building[outlier_mask]
                        # One more filter now, but only filters out the most egregious points
                        outlier_mask = geo_utils.is_not_outlier(
                            lidar_building[:, 2], thresh=LOOSE_FILTER_THRESHOLD)
                    lidar_building = lidar_building[outlier_mask]
                except Exception as e:
                    logging.warning("Could not retain interior lidar points. When shrinking polygon, turned to multipolygon")
            else:
                logging.info(
                    'Building %s has a unimodal distribution: %r', building['uid'], is_uni)

        return lidar_building, poly
