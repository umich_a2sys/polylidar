"""Extracts Buildings from OSM file and joins with LIDAR data

This module handles code for loading buildings from an osm file.
The building are then output into a dictionary with their geometry information
(points) projected onto the xy plane defined by the projection for the LIDAR data

"""
from __future__ import (absolute_import, print_function,
                        unicode_literals, division)

import os
import pyproj
from geo_utils.db import DBConn

class OSM(object):
    """ Class that parses database of OSM features """
    def __init__(self, bbox, file_path, projection='EPSG:3857', projection_local='EPSG:5555', table_name='osm_buildings', query_init=True, roof_shape='roof_shape', **kwargs):
        self.file_path = file_path
        self.projection = projection
        self.crs_data = int(projection.split(':')[1])
        self.proj = pyproj.Proj(init=projection)

        self.crs_local = int(projection_local.split(':')[1])
        # Get Buildings in bbox
        self.table_name = table_name
        self.roof_shape = roof_shape
        if query_init:
            self.osm_db = DBConn(self.file_path, crs_data=self.crs_data, table_name=table_name, roof_shape=roof_shape, crs_local=self.crs_local, **kwargs)
            self.bbox = bbox
            self.csr_bbox = int(bbox['projection'].split(':')[1])
            self.tables = { self.table_name: self.osm_db.query_bbox(bbox, self.csr_bbox)}

    def close_all(self):
        self.osm_db.conn.close()