import sqlite3


QUERY_ROOFS_BBOX_OPTIMIZED = """
SELECT {0} as uid, ST_AsBinary(GEOMETRY) as wkb, `{1}` as roof_shape, 
ROUND(y(st_transform(centroid(GEOMETRY), 4326)), 5) || ', ' || ROUND(x(st_transform(centroid(GEOMETRY), 4326)), 5) as centroid,
st_area(st_transform(GEOMETRY, :crs_local)) as area
FROM "{2}"
WHERE Mbrwithin(GEOMETRY, ST_Transform(BuildMbr(:minlon,:minlat,:maxlon,:maxlat,:crs_bbox), :crs_data))
"""


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

class DBConn(object):
    def __init__(self, db_path, crs_data=3857, crs_local=5555, uid='osm_id', table_name='import.osm_building_roofs_simple', roof_shape='roof_shape', **kwargs):
        """ Sets up Database connection and loads in the spatialite extension """
        self.conn = sqlite3.connect(db_path)
        self.conn.enable_load_extension(True)
        self.conn.execute('SELECT load_extension("mod_spatialite")')
        self.conn.row_factory = dict_factory

        self.bbox_query = QUERY_ROOFS_BBOX_OPTIMIZED.format(uid, roof_shape, table_name)
        self.query_dict = {'crs_data': crs_data, 'crs_local': crs_local}
        # print(self.bbox_query)
        # print(self.query_dict)


    def query_bbox(self, bbox, crs_bbox=4326, **kwargs):
        """ Queries building database and retrieves buildings in bbox"""
        curs = self.conn.cursor()
        query_dict = dict({'crs_bbox': crs_bbox}, **self.query_dict, **bbox)

        res = curs.execute(self.bbox_query, query_dict)
        rows = res.fetchall()
        curs.close()
        return rows

    def column_exists(self, table, col_name):
        """Checks whether a column exists on table """
        query = GET_SCHEMA.format(table, col_name)
        curs = self.conn.cursor()
        res = curs.execute(query)
        rows = res.fetchall()
        col_names = [row['name'] for row in rows]
        return (col_name in col_names)
